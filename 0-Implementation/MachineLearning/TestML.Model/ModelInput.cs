// This file was auto-generated by ML.NET Model Builder. 

using Microsoft.ML.Data;

namespace TestML.Model
{
    public class ModelInput
    {
        [ColumnName("Home"), LoadColumn(0)]
        public string Home { get; set; }


        [ColumnName("Away"), LoadColumn(1)]
        public string Away { get; set; }


        [ColumnName("H-Goals"), LoadColumn(2)]
        public float H_Goals { get; set; }


        [ColumnName("A-Goals"), LoadColumn(3)]
        public float A_Goals { get; set; }


        [ColumnName("R"), LoadColumn(4)]
        public string R { get; set; }


        [ColumnName("H"), LoadColumn(5)]
        public float H { get; set; }


        [ColumnName("D"), LoadColumn(6)]
        public float D { get; set; }


        [ColumnName("A"), LoadColumn(7)]
        public float A { get; set; }


        [ColumnName("O"), LoadColumn(8)]
        public float O { get; set; }


        [ColumnName("U"), LoadColumn(9)]
        public float U { get; set; }

        [ColumnName("TGE"), LoadColumn(10)]
        public float TGE { get; set; }


        [ColumnName("HGE"), LoadColumn(11)]
        public float HGE { get; set; }


        [ColumnName("AGE"), LoadColumn(12)]
        public float AGE { get; set; }

        [ColumnName("Draw"), LoadColumn(13)]
        public float Draw { get; set; }


        [ColumnName("H PROB NO MARGIN"), LoadColumn(14)]
        public float H_PROB_NO_MARGIN { get; set; }


        [ColumnName("D PROB NO MARGIN"), LoadColumn(15)]
        public float D_PROB_NO_MARGIN { get; set; }


        [ColumnName("A PROB NO MARGIN"), LoadColumn(16)]
        public float A_PROB_NO_MARGIN { get; set; }


        [ColumnName("Over 2.5 NO MARGIN"), LoadColumn(17)]
        public float Over_2_5_NO_MARGIN { get; set; }


        [ColumnName("Under 2.5 NO MARGIN"), LoadColumn(18)]
        public float Under_2_5_NO_MARGIN { get; set; }

       [ColumnName("HS"), LoadColumn(19)]
        public float HS { get; set; }
        [ColumnName("AS"), LoadColumn(20)]
        public float AS { get; set; }
        [ColumnName("HST"), LoadColumn(21)]
        public float HST { get; set; }
        [ColumnName("AST"), LoadColumn(22)]
        public float AST { get; set; }
        [ColumnName("HF"), LoadColumn(23)]
        public float HF { get; set; }
        [ColumnName("AF"), LoadColumn(24)]
        public float AF { get; set; }
        [ColumnName("HC"), LoadColumn(25)]
        public float HC { get; set; }
        [ColumnName("AC"), LoadColumn(26)]
        public float AC { get; set; }


    }
}
